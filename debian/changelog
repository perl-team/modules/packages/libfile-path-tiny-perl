libfile-path-tiny-perl (1.0-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-
    Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ gregor herrmann ]
  * Import upstream version 1.0.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.5.1.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Sat, 27 Feb 2021 02:26:38 +0100

libfile-path-tiny-perl (0.9-1) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ gregor herrmann ]
  * Import upstream version 0.9.
  * Add build dependency on libtest-exception-perl.
  * Declare compliance with Debian Policy 4.1.3.
  * Bump debhelper compatibility level to 10.
  * Add /me to Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Sat, 06 Jan 2018 23:10:12 +0100

libfile-path-tiny-perl (0.8-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ Lucas Kanashiro ]
  * Add debian/upstream/metadata
  * Import upstream version 0.8
  * Bump debhelper compatibility level to 9
  * Declare compliance with Debian policy 3.9.7

  [ gregor herrmann ]
  * Mark package as autopkgtest-able.

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Thu, 11 Feb 2016 10:59:55 -0200

libfile-path-tiny-perl (0.7-1) unstable; urgency=low

  * Team upload

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Nuno Carvalho ]
  * New upstream release
  * d/control:
    + update standards version
    + remove libtest-pod* from D-B-I, tests are not executed
    + shorten single line synopsis

 -- Nuno Carvalho <smash@cpan.org>  Thu, 26 Sep 2013 02:57:23 +0100

libfile-path-tiny-perl (0.5-1) unstable; urgency=low

  * New upstream release

 -- Alessandro Ghedini <ghedo@debian.org>  Fri, 07 Sep 2012 13:08:40 +0200

libfile-path-tiny-perl (0.3-1) unstable; urgency=low

  * New upstream release
  * Email change: Alessandro Ghedini -> ghedo@debian.org

 -- Alessandro Ghedini <ghedo@debian.org>  Tue, 10 Apr 2012 23:16:03 +0200

libfile-path-tiny-perl (0.2-1) unstable; urgency=low

  * New upstream release
  * Update debian/copyright format as in Debian Policy 3.9.3
  * Bump Standards-Version to 3.9.3

 -- Alessandro Ghedini <al3xbio@gmail.com>  Fri, 16 Mar 2012 19:18:01 +0100

libfile-path-tiny-perl (0.1-1) unstable; urgency=low

  * Initial Release. (Closes: #637292)

 -- Alessandro Ghedini <al3xbio@gmail.com>  Wed, 10 Aug 2011 12:03:13 +0200
